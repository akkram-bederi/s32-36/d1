const User=require(`./../models/users`)
const bcrypt = require('bcrypt')
const CryptoJS = require("crypto-js");
const jwt = require("jsonwebtoken");
const {createToken}=require(`./../auth`)

//REGISTER A USER
module.exports.register=async(reqBody) => {
	//return await .save()
	const {firstName,lastName,email,password} =reqBody

	const newUser= new User ({
		firstName:firstName,
		lastName:lastName,
		email:email,
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
		// password:bcrypt.hashSync(password, 10)
	})

	return await newUser.save().then(result =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}

//GET ALL USERS
module.exports.getAllUsers = async () =>{
	return await User.find().then(result => result)
}

//CHECK EMAIL
module.exports.checkEmail= async(reqBody) =>{
	return User.findOne({email: reqBody}).then(result =>{
		if(result){
			return true
		} else{
			if(result == null){
				return false
			}else {
				return err
			}
		}

	})
}

//LOGIN
module.exports.login = async (reqbody)=>{
	return await User.findOne({email:reqbody.email}).then((result,err) => {
		// console.log(result.password)
		// console.log(CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8))
		if(result == null){
			return {message: `User does not exist.`}
		} else {
			const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8);
			console.log(reqbody.password == decryptedPw)

			// console.log(decryptedPw)
			// console.log(reqbody.password)

			if(result !== null){
				if(reqbody.password == decryptedPw){
					return{token:createToken(result)}
				}else {
					return {auth:`Auth Failed!`}
				}
			}else {
				return err
			}
		}
	})
}

//RETRIEVE USER INFORMATION
module.exports.profile = async (id)=>{
	return await User.findById(id).then(result => {
		if(result){
			return result
		}else {
			if(result ==null){
				return{message:`user does not exist`}
			}else{
				return err
			}
		}
	})
}

//UPDATE USER INFORMATION
module.exports.update = async (id, reqBody)=>{
	const userData = {
		firstName:reqBody.firstName,
		lastName:reqBody.lastName,
		password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
	}
	console.log(`hi`)
	return await User.findByIdAndUpdate(id, {$set:userData},{new:true}).then((result, err) => {
		if(result){
			result.password="***"
			return result
		}else{
			return err
		}
	})
	console.log(result)
}

//UPDATE USER PASSWORD
module.exports.updatePw = async (id, reqBody)=>{
	console.log(reqBody)
	const decPw= {
		password: CryptoJS.AES.encrypt(reqBody, process.env.SECRET_PASS).toString()
		}
	return await User.findByIdAndUpdate(id, {$set:decPw},{new:true})
	.then((result,err) => {
		if(result){
			result.password="***"
			return result
		}else{
			return err
		}
	})
}

//UPDATE ADMIN STATUS TO TRUE
module.exports.updateAdminT = async(reqBody) =>{
	// console.log(reqBody.email)
	return User.findOneAndUpdate({email:reqBody.email},{$set:{isAdmin:true}},{new:true})
	.then((result,err)=> result)
}

//UPDATE ADMIN STATUS TO FALSE
module.exports.updateAdminF = async(reqBody) =>{
	// console.log(reqBody.email)
	return User.findOneAndUpdate({email:reqBody.email},{$set:{isAdmin:false}},{new:true})
	.then((result,err)=> result)
}


//DELETE USER
module.exports.deleteUser = async (reqBody)=>{
	return User.findOneAndDelete({email:reqBody.email}).then(result => {
		if(result){
			return true
		}else {
			if(result == null){
				return `User does not exist`
			}else{
			return false
		}
	}
})
}
